﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CookComputing.XmlRpc;

namespace OpenNebulaCSharpAdapter
{
    static class ExamplesClass
    {
        public static Array createOpenNebulaUser(string userName, string password)
        {
            //EXAMPLE FOR CREATING USER. USE SAME PATTERN FOR OTHER ACTIONS ON TOP OF OPEN NEBULA

             string rootMainUserHash = "userName:passWord"; //ADMIN OPENNEBULA USER THAT CREATES OTHER USERS. REPLACE WITH YOUR OWN, FETCH FROM DATABASE OR CONFIG FILE

             XmlRpcUserManagement xrum = XmlRpcProxyGen.Create<XmlRpcUserManagement>();

             Array openNebulaReturnArr = xrum.oneUserAllocate(rootMainUserHash, userName, password, "");

             return openNebulaReturnArr;
        }
    }
}
